import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  namespaced: true,
  state() {
    return {
      products: [
        {
          id: "c1",

          image:
            "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBUVEhgVFhUZGRgZGRoYGBgZGBgZHBocGhgcGhocGBgcIS4lHB4rHxgaJzgnLC8xNTU1GiQ7QDszPy40NTEBDAwMEA8QHRISHjQrJCs0NDQ2NDQ0NjY2NDQ0NDQ0NDQxNDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NP/AABEIANkA6AMBIgACEQEDEQH/xAAbAAABBQEBAAAAAAAAAAAAAAAAAQIDBAUGB//EAEMQAAIBAgQDBQQHBgQFBQAAAAECAAMRBBIhMQVBUSJhcYGREzKhsQYjQlJyksEVM2Ky0fAUwuHxY3OCs9IWQ1N0g//EABkBAQADAQEAAAAAAAAAAAAAAAABAwQFAv/EACkRAAICAQQBAwQCAwAAAAAAAAABAhEDBBIhMUETUZEiMmGBcaEFFFL/2gAMAwEAAhEDEQA/APZoQhACEIQAhCEAIQhACEIQAhCEAIQhACEIQAhCEAIQhACEIQAhCEAIQhACEIQAhCEAIQhACEIQAhCEAIQhACEIQAhCITACErVMbTXdh5a/KQniicgzeCn9bSG0uyaL8JQ/aB5Uqh/J/wCUiTiwNuwwvYC4PPrYG3iZG5DazUhM39qC18p9H622yxTxMfdO9tm3/LJtDazRhMr9rf8ADc622PW2xAtJTxK29Jx+Q/5pG5LsUzQhM8cWTmGXxX+l5PTx1NtnHncfOSmmKLUI1WB2jpJAQhCAEIQgBCEIAQhCAEIQgBCEIAQhCAEIQgCTm8fxMklb5bHblpOknCcSBWqwIIN+cpzScY2j1FWy6uMbmAe+39I84s/ey+ZA9GUiZlJzLtLEEczMbyyZdtRY/wAa22emR/FlP+YRRjG3+rPTL2ed+TnnFp4jqAfECPqMv3V9I9aSJ2ogfEk/YS2mgcDbbr0EBimvchT4up2NwNgfjIXYcgvoIiP1C+gk+tOuyNqLIxtjcBAe+3f/ABjqfUxTjza2dAP4co/Uxi1VH2V9JncS4g4HYIXwAH6Ty80mTtRfONI55vC5+SgSF+IvyVR3nX9ZzL4tye07HxY2jlqnqZ6WWSPO1M6XDcZKEktcnkBpOuoVMyqeoB9ReeXoSWFgTPTcEPq0BFjlW48hNWGTl2VzSRYhCEvPAQhCAEIQgBCJCALCJFgBCEIAQhCAEIQgCTG+kdJ2pgoqNY6h1J9Muo8psEzl+K45mbQkAHs2JBHf4yvJOMV9R6hFyfBiUa9xrR7Q/wDhqrUVfxI+V/KwklXiGGXR62Iptsc9IAfCm3z85BxDFO4s6rU7mUH1DafCZrYgrp7N0H/DZ0A8AjKJiebE/b4NMcMvJaxHH0Q9itTcdX7J/wAvyk//AKkTJc5D4Vl+Vj85lPjV5l+8sM5H5w0q8QxGHI2zeOHo39RSvI+h8qidjXFM1l+k9IgliqkX0z5vTKpkK/SqmdAhP/Wo/mtOOxHsrn6ofkK/ICSYOnhri6D8jH5mKiS4V7navx8AXIpIP48UnyRWlCtx2k5s1XDqO6s7n0Wjb4wwmJwSroozdFp4cj1ZCw+MjqY/D21R2bup4fLe2vu0FY+o5T3sgu6+Tyk34Y6lxDB37VZ37qdFt+5ixv8AlEv+1pizU8NXcdans6Sj8Wd0NvIzPo8RX7KVQe52VfyB7fCWcPjmDZlRQ33jlzeqqDfxvPKy4o+weKT8M676InEMWeolJEI0WmS3h28qg+Qt53M6qcBwvi7hwxYk/audx07p3lNgQCNiAfWbMU4yXBnnFxfJJCEJaeAhCEAIQhAEhEvC8AWEIQAhCEAWESEAWESEAa2xnn2MqFXOs9BO04bi3B6pYvTs4OtrhW+Oh9fKYtYm4qjRp2k3ZRTFDnJkrrMivSqoe1SdR1KNb8wFpAuMHWch7kzoJJ9HRBkbxlarQToJmU8UM1s1pJVrgfai210Koe6JvYD0kT00NrASpWxQ8YlPEjwnn9Hray9TwQPSFemlO1wNb7DoJCmI/jEocRxYzAZ76T344RCTbJamILGy6D4yxTIA3mLSxYY2Gp6DUzUoYas3u0X8SpUerWEhRnZMqRfwxuwA6z0/Aramg/hHynnHDuF1A4L2W2uUHMeW5Gg36mej4I/Vp+EfKdbR8J2c/UU2qLEIQm4yhCEIAQhCARgwvIwY689UVKQ+8W8YDFvIo9bh14sbC8EpjoRLwBkHoWEIQBJkILW6bTWO0x6NRXQMpuCLgiZ8/gsx9skc6SGpTVh2lVu4qD8xHs6W1YC/UwmSSNEaM3GYLDIjO9FLIrMxyLeygsbWGugM5h+MYGyH/CVGz5bBFpswzoHQFVe9yhDCw2PW4HX8Sol6NRF3dHUA7XZCBc+JnDLhcStbD13oV2KPmdFDPa6lnFi5X94zZcp9wKDqBNejwYcibn3/ADR4nOS6Y9eMcPfKFwmIYtcqFRmLBdCUCv2gDvaJ+2MBewwWIJOYAezJJKmzWBqa2OhtsZSqe2bCDDvhKyEoEZ0wzZFK4hqoCoCLqysM2o7Sg2Mkq4jEuAowuJISph2UtT7ZSkqCpmY6hnalSYgG3Y1PXX/qaW64+UV+vPqy2/GcKisx4biQqC7l6CgKALktmfTTXX/WdQmBw+jLRp6gEEU0vYi45X2nIVKGIeniaa4OrevUquGy0aJQVEy62chj9lidWVm5ztaNPKiqbXCqD5KBMeqw4YJbKv8Amy3HObbsVSANLDe1tB8Ixz8/1Ec3PzvIaumo7z8phLCszm17gc7nozf0AnX8OP1NPf3F3390bzkGGhsFOoAzbWFtfK3rOv4cfqaZ37C69eyJr03kozdItwhCbCgIRLwgCwhCAVbxbyMGKDLKMakSXheMBi3kUelMkzRQZGDFDSKPakSXgDI7zA43xpUJQMwIGy7k317Vja3d3zxOSgrZbji5ypHQVayqLsbD+9hzlF+LoNg3wH6zjTxwlspUk5M1y5OmnMiwFiPX0kfiwyBgCq9dSNtRoPe8+RmSWob+01x09dnXniq7ZW8QLiVMJSC01VbWA0G/kD0nlGO+leIeqFVcgVgGJJOZTsb6W023Oo8/WsMbrIlJyXI2KL4ITRLEk3tfQfAnu6eXfJ6hGpO2usGOsZWW6MOqsPUSl88EpUrIRxCkaftPaJkvbPmGW/S/XukTYoEAoQQRcMCCCO62lpy6MrUGqIjCnbB5uyQHdH+sKLbtdkqpPMjnFCB6SC1qdXFEqm31YBJUgbXZGNu+UZW+k6MeTPJ8I3miCc4UplKSVbezStiKd2NgFRXydrcWAAHhGexNZaa1Cxth6rqWuGzB1RHb+LKQZm9J+5RvOpp4uxsbXt52vvbmJJVxCKhdmCoNSx0AA6k7TnMFVL4imx3ODVie9nUmI2FepTxSooZhXouqMQA5RaT5CTp2sttZoxOW7bJ+DVizy67N/D4hHQOjBlYEgqb3sbG3mLeMr4iuSSq20O5a2xB00/pz8ZX4RVQYUPTDBWZ3Cva6s9Vsy6aBVYkAdBJ0vsGVuWW1ufW+pl34NSk3FDkT6v3QTe9m0AOYG58N/Kddw/8AdJrfsLr/ANInIqPqyAtzm0U/iG/UD42M6/A/uk1v2V18hNmnRVk6RYhCE0lIQhCAEIQgFC8WJCXnMsdeLeMvFvIJUh+aF4wGU8bxBUsoILnZb2tpe56aAnqZ5lJRVstgpTdLsl4ni/Z0nYEZspCA82t2Rbx+RnB4is7AO4sW17TLvz+za95u432j3ZnIJ0UBfc6gDUE9TbXy0xXwzA3Lgm/20yg25XCg3HO05moyb3+DsabF6ceeyrh6HaDm2mZQLW5qdHGhGg2vcnxhisQVw79iwtcG5GU5huNLeO/drFz1AcpRiDcBlp1AQNdMwS23MkWIB1mNxNnayLRYi93YZiG5/ZNh5j0lFUzRdnMUc5qZmIvmF9Lk3bY69O/nPeqGw8/nPEGVVqgXO4BA1F7i+3rf4T2+iLadL29TLvHJTPsdbWOvG16mVSx5Ana+wve3MAAm3QGQrj6Jp51rqF0F6oFOxLFQpBClWLAixF78pEYN20VSyJcMzcTgGXBrRBBdFpqDqAchBHhoomfWwTgPkCkLWWtTUm25u6E2souz2P8AFNjjTH2QKkj6yjsSDY1UFjbxsR4znMIjipTu3YFauFF9cx9sCD3AoxH/ADO6Zcqd9mTMluVexKmAcJSzBWYVzVcaEDPnva+9sw9JJxTDVGYNTClsj0zmbLYPlIa/OxTbneQ8bxLoWy57GkTdWACdsDObkcja4udZXx7v9d22GZMTpmNl9k6ImUX7PZJ2+9KYqTaZQ65RM2DqpUV6eRgtBaN3LD3WvewB6CWX4U7U6hR1DtXp1kzKcoamEsHtqQcp26ynhsUXqXuQ3tKKOtzoQtRXU9RdSe/Qy59H6aC7BWDulN3NgFe5chwRqzElgSfugcpbjUlK37F+CKcqJcNhDSoJTLBmu7MfdUlmZzYdAz28pa5XdQOVxrl9ZK6X20Ivb1G45iQpUJYAgbnNa+traeGo9ZoXZucaVIfVTsDVr2FyvvXJBIF9rkkX5XM6nA6UqY/gXv5DnOXxL2UknLewB3NyQBpzuTa2+s6bh+lFL6HIt/yibMHkpzcJFq8LxLwvNJn3C3heJeJmgbh14Rt4sDcUoXjbwlxzLHRY2EE2VeIY5aSZjqSQqjqSbDy1FzMP9qqzqrdm7NlNybnQdoebb9BzifSdm9tTVv3eh775iTb0WZlWiHS+xJLadTe2nMaKPAmczVZW57fY7miwxWNS8s169P7rZb2GbtD39B7psOkgo8OKjLmawuSbqdvxXPqZUPFKaUB7WyoVCkHe47+d9OWhvM6p9LUzFQpAIurZr3HoSNvhM/fJq2vok4tiPrAi0w2v2hYW3uCxGumwv8CRy/FMUEsrKGK6gsAwXWxJ5DS9rdDebScWLlmQJZtgbADsgEEWFx2fujpOd4pw6pUDMAoy3ViWGXmNFv3nwsO8CYtXyS40jF/xV3GhzFlLG+UHtakKLaW/mtPcsLjtLNy5/wBZ4ZVpdsLdb3AOv2SRpoD1noPBeMMoVKilgBo494AfeB97x0PjKtTNxpxKZ45vmPg75yrKbsCCCGsbHKVKtvtoSfITKb6NqtJadKoUyZwpqUlZPrRUVl9mgRRpUNspGwGtyDFhuIU3Iyut+QOh/K00FxLDS2nS7gegIHwjDrElU0Y5N39S5Jhhl9mqe8qlMubc5MpViRuSy5v+rykFTh6dmwsVd3Gp958+a9+XbbTvg2Ibr8BA4g93pK56jHLs9qUKpoo43h6tfPezIyGxsMpIJ13vpIn4YjlzYk1FZW10Ae2fL92+VST1E0WxB6D4xn+IIGgHxme4p8N0VuOO++COlw5BUL5buxVybn3kDKpte2zHxklHCJTJKJYta+rHYsQBcmwuzaCw1MjOKa/LbpI3xDX3+A75f68IqlZesmOPSLTD9ZVLqKmpHPn+A/KQO7HmZn18VTRiWdR3A3PIbLryHKef9m+kQ8zlxGJsVsYPs79T/SdPw8/Upf7i/wAonm78SzGyCw5sd/Icv70novDnvRQ/wL8p0dDKUm2yrMpxSlIt3hEvEvOjRm3DoXjbwvFDcOvCNvCKI3FS8LxkW8voyDwYsZeLeRQKPF66BchUMzAkA7Dvvv6TmkxaXCv9VqDZj2TvqrmwHLQ23l76U0Wz0nBsDdDr429cxlPKtVVV196wGncf0tORqZXkaa6O5pI7cSkn2Z30g4e70lVO0gzscoD3J1DCxtmGvMjTQTjTTKHJ2w1hcEBTnHIgC5sbd+nhOk4v9HqAu2m3gfS05XF0VDWFV+gs7W26AgenSUpq6s2xbrqy9gKVQPmUGwW1wpI1+zYcjax8799Pi+PCu4ZhofdYG55+6CDa43ty1lqllZSpqVWO9jUcg7nYs0xcTg1Wo4CgC/S9tOsmKhfJ5lKT8FOrjyblKdv4iNteQB126zvOC1UdA32iBp5aj1nDs+UkeunQ+Et8P4myHa6k3PceVozw3xW1dCDcXy+zunoA8o6mrr7ruv4WI+RkeC4glRAQwuQLi+x5y4hB5znSxyRdafYiYyuD+8J/EFb4kR7cVrjmp8VH6WklNACSTfujK2WeXF1yefTxvwvgibjOI6U/NX/R4wcaxHSn5K/6uYx1vGBLTzf4HoYvZEp4rXPNR4KP1vKOJxuIN/rWHcoVfioBmjSVecjxdEEdka/6z0r8IlY8a8L4MNldz23dvxMzfMyxQwndJfZMDqPiJapWuBzPIbnwHOekpSPcnGK4J8DhbsFAuSdhPSMJRCU1S+w+J1PlczF+j/CQgFRrFiOyBrbxPWb07mjwPHC32zh6zOpy2rpD7wvGQm2jDY+8LxkIFj7wjISaIsqxbxgMW8uorHAxbxt4XkEUVOL4X2lFlHvLZ1/Euo9dvOcqla1uVnvy0sSfladrecvx/hThs6bMTcAbXA/UGc3W4m6mv2dX/H5krxyf8HM/SPEN7NWGzF821tLWHn0+E5HNmF7H3QPh/tO8x/BzVoKl7fa8Lja/lMBPo04JBGgFttz5i0wKkrZ1k64Rk8Ooln1vbKTf9BeXMZQQh2JAsbfEf3/WX6PD2QlBpYb6cxfluNe6YHFUa9uZ3169+8R+pkSdGTigLm23z2mhg8JdQSNwJUWhtf8A21/1nVYPCgr4W+UnNNxikiMfLbZhVMKy9pdPAxo4liE2Y+c6hsKLWtKlThoPKURz/wDSLqTMZfpRXG4B/vukn/qxzuo8v6SzV4KDy+X6yjV4L3S1Twy7R5cZeCY/Sl/uRB9J3P2fjKbcJPfG/ss98msHsRtkaK/SOpsFA8SIPxyqd3UeGvwEp0+Ek9ZZp8I/s2/rIbxLonbIf+0yd2d/MqPhrLnCMQwfN1lalhQNJp4ajllUppfaHFVR230b4wQ+Rj2W+HfOxvPLMLUysCJ6Rw6oWpIx3I/2+E6mizOcWn4ONrsSg1JeS3eF4XiXm85wt4XiXheALCJeEEFK8cDI7xby4iiS8W8jvFvIA68UiNvC8hqyVwVMdhh7NyBqFJAva+UEgX3nK/tU2vlsTuCdf9/KdrecvxzhT5y6C6kDS+05urwUlKKOpos9txm+fBiu+fta3uee9hptv43MxOK4S651vddTtqL9Qb28jNQIy1ACGAyljcG19OdrX3j3oh6TjLdmFgLXPW+Uj42nP5tHTfRwP+IzMB3gD5TuMOAEGljqD4gzmsF9GsTUq5UpmwPO/W5OXceYAnf4zh2YaaG5+fWTqEklfkp9aMZUzIjysbWwrpuNOv8Art8ZEKhG4I/vrMDXsaYzUlwyzTA2kdegI7DMGJN4V7CKlRZfJRaiOkYaIj3qRiVI5JLFLDr3RuLIAsJJRUGQcSsqE36fMSVFkWrKaC0so95kjFai2t+msu0S52X1/pvPW1rsiTSRqUDt3z07ArakgH3F+QnmmCwbXDNp/fSekYE/VJ+Bf5ROr/jqtpHE1+RSpItwkd4XnVo5g+8Lxl4XigPvCMvCKBUheMvFvL6JHXi3jc0LyKA7NFDRsWKA4GKDI7wvIoDXwiHUoPS3yiLhUH2B8/gZJeLeV+nG7pFnqTqrfyPUACwAA6AWHoJmPQDC43miDKmHHZHn85h12NNJP8mrSLdaf4KTYU323kFTho3Kedv8y6fGbLWFydgCT4BSx+At5yGhjKTrmDKAdLkZGv2rj7Nj2GPO9jynLWjTTaZoktr4ZhnhyAnQjzv8wZXrcKU8/gP0tOjr0BeQNQHxmeWGceCyPq1aZy78AHJvg36PI14AL+8PRv8AynTvSA5/CRrTBG/LpK6ndE+plurMOnwoDn8P9YlbhCNuTy0AUc79CZu/4cX8ukRqAv8A7T36eRoP1n5MJeFUh9m/iSfht8JYwyKrgBQQDqtrA7Eg27jNNqQkSJeppbnroAPc3JnqGCV3JkPFN/cyetg1phmIuGIFO/3SQS1uoBC+Ld03sISaa239mhAPUoN/75THdaVjnZjpqEX/ADPYHyvNjDsuRcqGxUbsAbWFrgC17Tr6SCi3X9FGeMVFKvklJIHa3JAFwASbi9gAOVz5RyKb6j7TDawtmIHwkIC3uFsevZ+Y1+EX2PPKPEC/xE3pMxuvb9igtvZ/yoB87xarbZdC19fugWvbqdRbz6as9mPuj0g5CnUdrkPtEeHTvOkVSpsXbtL+iS8JGGhLaKCreLeNhLiB94XjYCCR4MXNGGLAH5oBowxZAHXheNhIokfK2HPZHdf5yeRUfd9Zz9d4/Zu0Xn9D3N9OoIPLcdZXPDqYSysARmsHVdMwcNdQFJFnbb9Td8t0/c8xOfGTSZsmjOxlX2dNedmpprp7zJT9bW85nniV8th71Wom/JDU12/g275Y49+6/wD0o/8AeSYGG95P+fV+eImfLJ8lOTI40kXsdjwhN+SM/LkQLD1kVbiAQvoewrty19nlzAebDeVON/b/APrv/wBxJDxD/wB38GM/npzPGKbRnc3Zr0eKA1CltnRL975vllkuDxudjoLZVYdq51LDUWGU2ANu+YOC/eH/AJtL+arNXgv7sfgT5vNEZPo1Ycjk1ZfdrC/j85WCkHN0N7eOkfiOXiY7+o+UsiaZDsQ11vfcDXzE3MIfq0/AvyE5yv8Aux4L81nQ4X92n4V+U6Oi7Zh1n2omzQDEbEjwJHyjTEM6O1M51tdEjVGO7N5Mw+RjFAGwA/vnCEjZFEucpdi3hEhPVEH/2Q==",
          description:
            "Bees also produce a compound called propolis from the sap on needle-leaved trees or evergreens. ",
          area: "propolis",

          price: "5 eur",
        },
        {
          id: "c2",

          image:
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQoZJ_f8OPVc2ptirqOzwCupJJOhBX55dE28w&usqp=CAU",
          description:
            "Many of us enjoy honey and incorporate it into our daily diet . ",
          area: "honey",

          price: "10 eur",
        },
        {
          id: "c3",

          image:
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS0hajgV304bb97QoOQ4PD17nAhHhinsuWHvw&usqp=CAU",
          description:
            "Beeswax candles emit light that is similar to the light spectrum of sunlight. ",
          area: "candles",

          price: "5eur",
        },
        {
          id: "c4",

          image:
            "https://cdn.friendsoftheearth.uk/sites/default/files/styles/body_text_image/public/media/images/world%20without%20bees.jpg?itok=mPWNlJbT",
          description:
            "The Bee Book shows you step-by-step how to create a bee-friendly garden ",
          area: "honey book",

          price: "5 eur",
        },
        {
          id: "c5",

          image:
            "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxITEBISExEVFRUVFRUVFRUYEBAQFQ8QFREWFhUVFRUYHSggGBolGxUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OFxAQGisdHx0tLTctLSsrKystKy0tLSsrNystLS0tLS0tLS0tKy03LS0rLSsrLS0rKy8tMC0tLS0rK//AABEIAPYAzQMBIgACEQEDEQH/xAAcAAEAAgMBAQEAAAAAAAAAAAAAAwUCBAYBBwj/xAA+EAACAQIDBQYDBAkDBQAAAAAAAQIDEQQhMQUSQVFhBhMicYGRMqGxI1LB0RRCYnKCkuHw8RWy0gczNEPC/8QAGgEBAQEBAQEBAAAAAAAAAAAAAAEDAgQFBv/EACURAQEAAwABBAIBBQAAAAAAAAABAgMRMQQSIUFRYRMicYGh8P/aAAwDAQACEQMRAD8A+4gAAAAAAAEdeqoQlN6RTk+OSV2R4+rKFOUoq7S0145u3Gyu/Qo6OKjUV3Nu/FtgcxtXtTjHF3+ycs1GKu6UGsk5auVtXpe5T7N2zi4yc3iakYxzcpSlUTWvwyun8zsKM6cqkk4rplw6E+L2RSlFqUI2a4Jfhr6nz89G65XKZ/4erHbhMeXFV0u3NXuu9nGMI6Q8PirNatRv4UWWzO1s6iTlS3E9Hqpel7ooNobDpq0pZ7llCP6sVfW3oik2/tas2qWHsqiUb7sVeWt9dHbNs8F9f6jDd7Mp8/vxxpNOGWPY+p4XbCbW8suavl6MtTmOzWHqVaNKpVssvElnvyjk/JZHTn3MMvdjMp9vFlOXgADtAAAAAAAAAAAAAAAAAAAADGc0lduyAyNOvs6i25OCT4yXg9W0amM2nN5Ul/E/wX5lXXwlWpnKo30vp6AY7Zw2HpwlONS8lpG6km780iqwu2k1rb5kuM2FUkrKWp5s/sok7zdzl0xxlXfg7N5rWzINk7HnUs1VpRT1bT3vpm/U639Ap7m7bK3Q0sPsiEJXi2ly4Hk3ej1bsvdnOtMNuWE5F/s/DRp0oQi7qKtfm+L9zYKmN1o7GxSxr/WXqvyPZjJJyMq3gYwmmrp3MioAAAAAAAAAAAAAAAAAACHFYiMI3fouLfJFHXxEpu704LgiPaGK7yo3wWS8ufqe0mFTwJIkcUSJkGSMkYpmSA9sz1RPUxccHjRjJGbZqVMXFStfMCenUcXde3Ms6FVSV1/hlSppozwlbdmuTyZUW4AAAAAAAAAAAAAAABr4/EqnTlJ8sur4Gwc5tzEqc9xPwpe7eoFbhqhu0ygqKdJ5puHCSz3ekly6lhhcYmlmRVtGRLGRq0qyZPGQEyZkiNMzTAzPTYg5WXij8vyNeo83xAxkz5xj9tWxdaN/hm1ryyPosmfmrtBt9rH4q1//ACKqVuNqskhR9x2VtPe4ly617HzrsBsvH1Pta32NK3gjKnerU/aav4I+eb5Lj9Bw2z91pym5W0W6oq5B0WGrbyvx4+ZKVGFr7s+jyZbnSAAAAAAAAAAAAADT2riNyk2tXkvN/wBDk3LMs9v4q9Td4Ry9eP8AfQrkrgbGHmR1dkU5Zwbpvpbdb6xeQhBo2KcyK0/0KvDTdmuj3X/LLL5mUcZOPxU6kf4JNe8bos4TJozAqYbVh99erSJ47Rh95e6LG6eqXsO7j9yP8qA0f0+H3l7o8/1KH3k/VMt4YJWuoQ9keVI7rtkvICqVac8oRl+84uKXXPUq9hdg8FhZd5CkpVW23Vm+8qOT1ab+G/Sx07mYNgLJaEU5nspEcoFGPEt9n1bwtxWXpwKlk2z627NcnkEXQAAAAAAAAAAEeIq7sJSfBXJCo7RVrQUeevkgOfrVN6Tb4u5jF2MHzJaTCtmlM2IJM1oQRNGLIJ4w6kqT6EMJEqkUZJMyV+RjGZIphEkaM2tPmjydOS1+tzOOKSXwx9jCrXvwS8grHdfM83UeOZ5vAesimzNpmDigIln5HrYnIx6gX+GneEX0+ejJTR2VUvFrl9GbwQAAAAAAAAOc7SVPGlyj9X/g6M4/bFW9WfR29Fl+AGnCRLGHIi3SSmyKng2jZpzIacieKQE8GZ26EMaa5sz3HzKJFFHqiiNRZ5T3rvlw/EDdjRp2zk/ZnlSELZXf0IEpaWJO6fNeV7/QDzLkHI83WebnUDyUiN3ZLkYSkBFKJhKRlIxA3tkytO3NfP8Au5blBgJfaR87e5fhAAAAAAAAHknZXOHxUryb/u52WLnaL8n9Di668T8wMYmxTtxIIMmgiK2I0+TJFFkUWSwkBJCbRL3ghUMroootv9p6WCg5121FvwOMXJyk8923u/Q4rE/9UK3eV3QlSowpQjJwxC3KuJnxhTjztw69cup7ednVi6UFGe5KnJVIu17OKfC/XU4vD7Mw9Oq8VjZrE1ZSi7zirSskopQVk8or24Hk9R6rDRzvzb4ka6tV2eH0zAbaVbD4evubjq0o1JQz8Dau0ulvqTx2i0kpSSvwXBPm/lY43Abep1XKnR3rWvHesm43zvbQvMJTjbVTfNvdhF9FxfuTR6i7cfdzl/C7NXsvHROre2Zk2auEr3SikssnbNL15m1GpkeuVix3WeOK4mTkYMoxkyNokkiKQHtKdpLodKcxHU6aDuk+gR6AAAAAAADTx78MvJ/Q4+pqddjtH5P6HJ1dQPIomiQQNiEiKmhIljYhiiSMeoE24rZMjozyb4+56kyKrSebXquDA1a3ilZ29E1u+t9ThtubDcqtrqz01yz4HcuLcHayztoslxZUbYwrcXOnk4rJtOW9d2cmr552Pm+u1ZXD3YeY9Pp8/bl/dW7PweFw0G51d2btGF91b82/hTbVja2BtGVSct6DpxUt2Ddrzyvlf4l1Oeo4LC0ZyxGKqPEVNYRmotLLSNPThq9DYlQr15QxVdqhRp+KFPxKW7HNOSys/wDFs7nydduvmyZdv35kv65/rr05/wBV5Y+i0IZqzV/3km11VzejSSRz2zFKrRhUjGMlJXzum88ne2pv0cXKOTTaWq1cPzR+j15+7GXnOvBljy8WbtyMJSPINNXvketI2cI2YSyM5SImuZBitTp6XwryX0OXTzOopqyS5JfQqMgAAAAAAAV2OZy9eOZ1G0o5M5mugI4MmgQxJYxIqaKZIpEUZEsZgSRqEiqEaaMlFAasouLeV08zKnRus8lbQ2e7R5OkrPMlg47a/ZyM3GsoQlJtZNe3mVv+lYqvUjCtFd1H9SLn43+07Kyt9Dt8NNPuU+qfSUY2/MslTR4svRYZ5e75l/7w3m6ycRbPo93TjHkjzE0896Ov+5GxZHt0ezHGY4zGfTG3t6hoVE9FZ8VyM3HmYzte/HmeObeiOkeyaRrSndksoc2a1arwQEkHmdUkcvs+N6kfNHUiFAAVAHjZp1dqU1xb8lf5ktk8rzrdBqYfaFObsnZ8nlc2xLL4RrY6neJyVVZyT4M7Vo5jbmF3Kin+rLJ9HwKKyxJBmEkZQkRU8JkiSIo2ZkogSbnU93HzMMx3j5AZ2kHGRH+kHqxPUioYQa3Mst9v1d0WCUuhp1K97dGn8yaOJJPgT93Lme90uLZB375M9vN8LeZeie0VwI6ldIjcOcvYjlOK0Q6ceVJt9DXlyXuezqt6e/A8o08ydVbbBpeO/JP8vxOgK/Y9G0W+f0RYNnUc0I69aMIuUnZLNs1sRtSnHjvPks/noUO1tod9HdlFKKd7Xeb6nOWyRccbU2Jx8qr1tHgvxfNkTRV4dqElb4W7NcE3o0WcmeTty+a35xHKVi/2Pi9+DTeccvNPT8fY5yoWmwaTam9PhXm8/wChrp71nn4X5Di8NGpBwkrpqxMD0snF4vCSpS3J/wAEuFRf8uhGjs8Vho1IuE4pp8PxXJnN43ZNSm7xvUhzWc4Lqv1l1WZFakUZowg080yRRAb7Pe9GZ42AdZBVF0MGo8jy0ORFSqquhksQuhCow5GcXDkiKyeK6njqyfM971ch3jekWTow3JPp8zxwS1dyV05cWoow3Yrr1ZLV4wzfkbmDpxv4mklrz8kuJpzqdSCeIM7tkdeyugxG24xVqcdOLyXoioxe0pz+KTfTRexXzqM8jFvRGd2ZZOpjIznXbI2zdwuzJz0T/vqXOE7PpZzfovzOsdOV8pdknhzKu2kl59CylVRdVuzmHlrF+k2j2l2bwsf/AFKX7zlP5Nms08+3H8ijwse9luwz5tZqPm9EdZhMOqcFFcNXzfFmdKlGKtFJLkkkjM0xwmLi5dAAdIAADTxezKVR3cbS+9Hwy9WtfUrquxZr4JqS5S8D91k/ZF6AOZqYWpHWnL0W/wD7bkDktHbyeT9i529tmGGp7zTlLRRX1k+COSj2gWJluzbTekH8PkufrmZ55zF3jja2f02lJ2jKMnyUkzS2hWrR8VOnGUVqry3zU2psFSW9T8M1mkslJ9OTPNgbUc/sqn/cWjeW+lrf9pHmu21tMI2cBj4VotRbjJaxy3o9VzRE8XVozSq+Km9KiVnF8pJEG28E4S/SKWUk7yt9fXRlpg6sK9FStlJZr7suKOO38uuRZUcQrLJdGks0J135FLstuE50G/h8UHzg+Hp+Jt1ajud/yXjn2RsTrEMqzIXIloYeUnZJsTHLItkRykexpt8P78i7wWwJazdvqXWGwFOGkc+bzNsdE+2d2fhzmD2JOWbVlzeXyLvC7Hpx18T+XsWQNpjJ4Z22+XkYpZJWPQCoAAAAAAAAAAAU/aHbHcRio/HO9v2UtWXBxXbaV6tNrTca9VL+qOcryLPKGjXU7u7b1d3dt9eZXbT2KpeOn4Za7qVrvpyIcPWcZJr/ACi57zRrjmePZ8fL0YtTZGOdSDjL445PquDK3tJhHBxrwyaav+9wfqT4v7LFU6i+GplLz4/gyx2nT3qNSNtYu3mldfM4/bso1o1KSlbKcc1yurNfUp9gVHTrVaL0u2vNfmvoOy1e9KUfutNeUl/QxxHhx1N/eS+acfwEG7tCe7iKE+d4vqnZf/Ra0MDOq/CunQqNowc6uHild7zfot1s+g7Jwnd0lF6vOXm/6WNtWPay2XkV+D7PxWc3foXFGhGKtFJEgPWwAAAAAAAAAAAAAAAAAABjNXTRwnaajJTz9PxO9K7bWzVWptL4lo+vIlnYsvHzdvMs9n1LwtyfyK3E03Cbi001qrG1s52Tvx0PLnPpvjUPaWeVLnvfTX6otac7xi+aT90Uu2PtKlOK/Vvd8E21q/JfMsnvypvuYuo4qy3bSs7WvrwM5jb4dW8U3ZhWdS2lkvm7G5OO9iov7qSXWWbSX8xY9nOz1fJOnuQveTk/FLoktPc7HAbDoUm5Rpreebbu8/U2w037cZbPw0tlbBSkqs5Xdsla1i/APRJzwxt6AAqAAAAAAAAAAAAAAAAAAAAACn25sCniFd+Ga0ktfUqMF2PcZKTrt24cD0AWNXsnhJS3pUlvcbZKT5tFngtnUqStTgo+SPABtI9AAAAAAAAAAAAAAAAAAAAD/9k=",
          description:
            "Check out our ceramic bee selection for the very best in unique or custom, handmade pieces from our shops. ",
          area: "honey ceramic",

          price: "5eur",
        },
        {
          id: "c6",

          image:
            "https://www.theluxecompany.co.uk/images/bee-ceramic-bowl-p964-9052_zoom.jpg",
          description:
            "Check out our ceramic bee selection for the very best in unique or custom, handmade pieces from our shops. ",
          area: "honey ceramic",

          price: "5 eur",
        },
        {
          id: "c7",

          image:
            "https://www.worldbeeday.org/images/did-you-know/2018-05-honey-other-bee-products/slika-2.jpg",
          description:
            "Check out our ceramic bee selection for the very best in unique or custom, handmade pieces from our shops. ",
          area: "polen with propolis",

          price: "10 eur",
        },
        {
          id: "c8",

          image:
            "https://st.depositphotos.com/1003169/4072/i/950/depositphotos_40729773-stock-photo-honey-bee-products.jpg",
          description:
            "There are several health benefits that honeybee products such as honey, propolis",
          area: "honey polen propolis",

          price: "20 eur",
        },
      ],
     
    };
  },
  mutations: {
    findProducts(state, payload) {
      state.products = payload;
    },
  },
  actions: {},
  modules: {},
  getters: {
    products(state) {
      return state.products;
    },

    HasProducts(state) {
      return state.products && state.products.length > 0;
    },
  },
});
