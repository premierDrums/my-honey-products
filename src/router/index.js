import Vue from "vue";
import VueRouter from "vue-router";
import SectionProducts from "@/components/products/SectionProducts";
import SectionHeader from "@/components/SectionHeader";
import ProductsList from "@/components/products/ProductsList";
import InfoProd from "@/components/products/InfoProd";
import TheMembership from "@/components/support/TheMembership";
import QuestOne from "@/components/quest/QuestOne";
import QuestTwo from "@/components/quest/QuestTwo";
import QuestThree from "@/components/quest/QuestThree";
import QuestFour from "@/components/quest/QuestFour";
import QuestFive from "@/components/quest/QuestFive";

import SecondaryQuest from "@/components/quest/SecondaryQuest";
Vue.use(VueRouter);

const routes = [
  { path: "/products", component: SectionProducts },
  { path: "/header", component: SectionHeader },
  { path: "/", redirect: "/header" },
  { path: "/list", component: ProductsList },
  { path: "/info", component: InfoProd },
  {
    path: "/hive",
    component: TheMembership,
  },
  {
    path: "/quest",
    component: QuestOne,
    children: [
      { path: "/QuestTwo", component: QuestTwo },
      { path: "/QuestThree", component: QuestThree },
      { path: "/QuestFour", component: QuestFour },
      { path: "/QuestFive", component: QuestFive },
    ],
  },

  { path: "/secondary", component: SecondaryQuest },
];

const router = new VueRouter({
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    } else {
      return { x: 0, y: 0 };
    }
  },
  routes,
});

export default router;
